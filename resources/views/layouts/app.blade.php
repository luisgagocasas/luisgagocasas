<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')
  <body @php(body_class())>
    <div id="wrapper">
      <div id="sidebar-wrapper">
        <div id="main-sidebar">
          <div id="profile">
            <img id="me" src="@asset('images/common/luisgagocasas.jpg')">
            <p>Luis Gago Casas</p>
            <p class="subtitle">Frontend <b style="color:#16A085">/</b> Developer</p>
          </div>
          <div id="mainMenuContainer">
            <ul id="mainMenu" class="sidebar-nav">
              <li data-menuanchor="homePage"><a href="#homePage">Home</a></li>
              <li class="navSubtitle"><span>Where it Begins</span></li>
              <li data-menuanchor="aboutMePage"><a href="#aboutMePage">About Me</a></li>
              <li class="navSubtitle"><span>Who am I?</span></li>
              <li data-menuanchor="portfolioPage"><a href="#portfolioPage">Portfolio</a></li>
              <li class="navSubtitle"><span>My Work</span></li>
              <li data-menuanchor="contactMePage"><a href="#contactMePage">Contact Me</a></li>
              <li><span>Get in Touch!</span></li>
            </ul>
          </div>
          <div id="footer">
            <div>
              <a href="https://fb.com/luisgagocasas" target="_blank"><i class="fab fa-facebook"></i></a>
              <a href="https://twitter.com/luisgagocasas" target="_blank"><i class="fab fa-twitter"></i></a>
            </div>
            <div id="copyright" class="center-text">
              <span>Arequipa - Perú</span>
            </div>
          </div>
        </div>
        <div id="toggle-btn">
          <button type="button" id="menu-toggle" class="btn btn-default">
            <span class="fas fa-bars"></span>
          </button>
        </div>
      </div>
      <div id="page-content-wrapper">
        <div id="fullpage">
          <div class="section">
            <div id="carouselExampleIndicators"  data-interval="8000" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
              </ol>
              <div class="carousel-inner" role="listbox">
                <div class="carousel-item active" style="background-image: url(@asset('images/background/inkalabs.jpg'))">
                  <div class="carousel-caption d-none d-md-block text-black">
                    <h3>InkaLabs</h3>
                    <p>The people in this photo I value them very much for how much I have learned from them.</p>
                  </div>
                </div>
                <div class="carousel-item" style="background-image: url(@asset('images/background/teacher.jpg'))">
                  <div class="carousel-caption d-none d-md-block">
                    <h3>Online course</h3>
                    <p>Something I like very much is sharing what I've learned.</p>
                  </div>
                </div>
                <div class="carousel-item" style="background-image: url(@asset('images/background/friends.jpg'))">
                  <div class="carousel-caption d-none d-md-block">
                    <h3>Edy, Sandra y Luis</h3>
                    <p>No doubt there is always room for good friends.</p>
                  </div>
                </div>
                <div class="carousel-item" style="background-image: url(@asset('images/background/pataccala_aqp.jpg'))">
                  <div class="carousel-caption d-none d-md-block">
                    <h3>Pataccala AQP</h3>
                    <p>A project that made me value the culture and places of my city and my Country.</p>
                  </div>
                </div>
                <div class="carousel-item" style="background-image: url(@asset('images/background/maestro21.jpg'))">
                  <div class="carousel-caption d-none d-md-block">
                    <h3>Maestro21</h3>
                    <p>Teaching is a virtue of each person.</p>
                  </div>
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
          <div id="aboutMe" class="section">
            <div>
              <div class="container">
                <div class="row">
                  <div class="col-12">
                    <h1>About Me</h1>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 col-md-2"></div>
                  <div class="col-12 col-md-8">
                      <p class="text-center">I am always proud of the place where I come from the beautiful beach of <strong>Camaná</strong>, a province of the department of Arequipa - Peru.
                      <p class="text-center">The course of the university was fundamental for me and to know what I would do and this is precisely where I had the great honor of working and studying and so I realized that I was focused on the web to build from webs to very advanced systems.</p>
                      <p class="text-center">My work beginnings were fundamental to be able to value the price of time. and that this should be exploited since it is a non-recoverable resource.</p>
                      <p class="text-center">There is no doubt that I must recognize and thank two very indefatigable accessories and that I get a lot of benefit from knowing their architectures and applying for projects. Thanks to <strong>WordPress</strong> and <strong>Moodle</strong>.</p>
                      <p class="text-center">Now I know that knowing <strong>frontend</strong> and <strong>SEO</strong> can build many great things from my personal projects and for my clients.</p>
                      <p class="text-center">
                        <img src="@asset('images/background/luisgagocasas_camp.jpg')" class="col-6" alt="Luis Gago Casas">
                      </p>
                  </div>
                  <div class="col-12 col-md-2"></div>
                </div>
              </div>
            </div>
          </div>
          <div id="portfolio" class="section">
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <h1>Portfolio</h1>
                </div>
              </div>
              <div class="row">
                <div class="col-6 col-md-4">
                  <div href="#" class="thumbnail">
                    <img src="http://placehold.it/360x400" class="w-100">
                    <div class="caption">
                      <h3 class="title">asdasdsa</h3>
                      <p>as dasd as das as</p>
                    </div>
                  </div>
                </div>
                <div class="col-6 col-md-4">
                  <div href="#" class="thumbnail">
                    <img src="http://placehold.it/360x400" class="w-100">
                    <div class="caption">
                      <h3 class="title">asdasdsa</h3>
                      <p>as dasd as das as</p>
                    </div>
                  </div>
                </div>
                <div class="col-6 col-md-4">
                  <div href="#" class="thumbnail">
                    <img src="http://placehold.it/360x400" class="w-100">
                    <div class="caption">
                      <h3 class="title">asdasdsa</h3>
                      <p>as dasd as das as</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="contact" class="section">
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <h1>Portfolio</h1>
                </div>
              </div>
              <div class="slide">
                <h1>Interlocked Slides</h1>
                <p>Want to apply it to all sections</p>
              </div>
              <div class="slide">
                <h1>Slide 1.2</h1>
              </div>
              <div class="slide">
                <h1>Slide 1.3</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @php(wp_footer())
  </body>
</html>
