// import external dependencies
import 'jquery';
import fullpage from "fullpage.js/dist/fullpage.extensions.min";
import "fullpage.js/vendors/scrolloverflow";
import "fullpage.js";

import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
// import {  } from "@fortawesome/free-regular-svg-icons";
import { faBars } from "@fortawesome/free-solid-svg-icons";
library.add(faFacebook, faTwitter, faBars);
dom.watch();

new fullpage("#fullpage", {
  licenseKey: "OPEN-SOURCE-GPLV3-LICENSE",
  anchors: ['homePage', 'aboutMePage', 'portfolioPage', 'contactMePage'],
  menu: '#mainMenu',
  sectionsColor: ['#FFFFFF', '#FFFFFF', '#e4e4e4', '#e4e4e4', '#2a7a9e'],
  css3: true,
  scrollingSpeed: 1000,
  navigation: false,
  slidesNavigation: false,
  responsiveHeight: 330,
  dragAndMove: true,
  dragAndMoveKey: 'YWx2YXJvdHJpZ28uY29tX0EyMlpISmhaMEZ1WkUxdmRtVT0wWUc=',
  controlArrows: true,
  verticalCentered: false,
  scrollOverflow: true,
  scrollOverflowReset: true,
  scrollOverflowResetKey: 'YWx2YXJvdHJpZ28uY29tXzlRaGMyTnliMnhzVDNabGNtWnNiM2RTWlhObGRBPT14Ykk=',
  //
  scrollBar: false,
  easing: 'easeInOutCubic',
  easingcss3: 'ease',
  autoScrolling:true,
  touchSensitivity: 5,
});

// Import everything from autoload
import "./autoload/**/*"

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
