export default {
  init() {
    $("#menu-toggle").click( function (){
      $("#wrapper").toggleClass("menuDisplayed");
    });

    $("#navAboutMe").click( function (){
      $("#bs-carousel").toggleClass('hidden');
    });
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
